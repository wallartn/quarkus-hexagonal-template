package dev.example.template.domain;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.enterprise.context.Dependent;

@Dependent
@RequiredArgsConstructor
@Data
public class Account {

    Long number;
    String name;
}
