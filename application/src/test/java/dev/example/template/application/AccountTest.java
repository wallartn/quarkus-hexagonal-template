package dev.example.template.application;

import io.quarkus.test.junit.QuarkusTest;
import dev.example.template.domain.Account;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

@QuarkusTest
public class AccountTest {

    @Inject
    CreateAccount service;

    @Test
    public void accountExists() {
        Account account = service.create(0125L, "beep");

        Assertions.assertEquals("beep", account.getName());
        Assertions.assertEquals(0125L, account.getNumber());
    }

}
