package dev.example.template.application;

import dev.example.template.domain.Account;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CreateAccount {

    public Account create(Long number, String name) {
        Account account = new Account();
        account.setNumber(number);
        account.setName(name);
        return account;
    }

}
